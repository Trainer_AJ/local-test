terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.87.0"
    }
  }
}

provider "azurerm" {
 client_id = "xxxxxxxxxxxxxxxxxxxx"
 client_secret = "xxxxxxxxxxxxxxxxxxxxxx"
 subscription_id = "xxxxxxxxxxxxxxxxxxxxxxxx5"
 tenant_id = "xxxxxxxxxxxxxxxxxxx"
 features {
   
 }
}


resource "azurerm_resource_group" "example" {
  name     = "example"
  location = "West Europe"
  tags = {
    "name" = "AJ",
    "Training" = "Azure Masterbatch"
  }
}



