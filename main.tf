terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    azurerm = {
        source = "hashicorp/azurerm"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
access_key = xxxxxxxxxxxxxxxxxxxxxx
secret_key = xxxxxxxxxxxxxxxxxxxxxxxxxxxx

# Create a VPC
resource "aws_vpc" "example" {
  cidr_block = "10.9.0.0/16"
  tags = {
    name = "Terraform-vpc"
  }
}

resource "aws_vpc" "example2" {
  cidr_block = "172.9.0.0/16"
  tags = {
    name = "Terraform-vpc-2"
  }
}

